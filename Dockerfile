FROM debian:bullseye as openssl
LABEL maintainer="Léon Lenzen"

ENV VERSION_OPENSSL=openssl-1.1.1k \
    SHA256_OPENSSL=892a0875b9872acd04a9fde79b1f943075d5ea162415de3047c327df33fbaee5 \
    SOURCE_OPENSSL=https://www.openssl.org/source/ \
    OPGP_OPENSSL=8657ABB260F056B1E5190839D9C4D26D0E604491

WORKDIR /tmp/src

RUN set -e -x && \
    build_deps="build-essential ca-certificates curl dirmngr gnupg libidn2-0-dev libssl-dev" && \
    DEBIAN_FRONTEND=noninteractive apt-get update && apt-get install -y --no-install-recommends \
    $build_deps && \
    curl -L $SOURCE_OPENSSL$VERSION_OPENSSL.tar.gz -o openssl.tar.gz && \
    echo "${SHA256_OPENSSL} ./openssl.tar.gz" | sha256sum -c - && \
    curl -L $SOURCE_OPENSSL$VERSION_OPENSSL.tar.gz.asc -o openssl.tar.gz.asc && \
    GNUPGHOME="$(mktemp -d)" && \
    export GNUPGHOME && \
    gpg --no-tty --keyserver keys.openpgp.org --recv-keys "$OPGP_OPENSSL" && \
    gpg --batch --verify openssl.tar.gz.asc openssl.tar.gz && \
    tar xzf openssl.tar.gz && \
    cd $VERSION_OPENSSL && \
    ./config \
    --prefix=/opt/openssl \
    --openssldir=/opt/openssl \
    no-weak-ssl-ciphers \
    no-ssl3 \
    no-shared \
    enable-ec_nistp_64_gcc_128 \
    -DOPENSSL_NO_HEARTBEATS \
    -fstack-protector-strong && \
    make depend && \
    make && \
    make install_sw && \
    apt-get purge -y --auto-remove \
    $build_deps && \
    rm -rf \
    /tmp/* \
    /var/tmp/* \
    /var/lib/apt/lists/*

# This Dockerfile builds Unbound --with-pythonmodule support and includes a simple Hello World style Python
# module to demonstrate the --with-pythonmodule functionality.
# See: https://unbound.net/
FROM ubuntu:21.04
ARG UNBOUND_VERSION=1.13.2
ARG DEBIAN_FRONTEND=noninteractive


RUN apt-get update && \
apt-get upgrade -y && \
apt-get install --no-install-recommends -y \
	build-essential \
	ca-certificates \
    git \
	dnsutils \
	libevent-dev \
	# libpython3 \
	# libpython3-dev \
	libssl-dev \
	python3 \
	python3-dev \
	python3-distutils \
	python3-setuptools \
    python3-pip \
    rsyslog \
    iputils-ping  \
	swig \
	vim \
    libnghttp2-dev \
	wget && \
	rm -rf /var/lib/apt/lists/*

WORKDIR /opt

COPY --from=openssl /opt/openssl /opt/openssl

RUN wget "https://www.nlnetlabs.nl/downloads/unbound/unbound-${UNBOUND_VERSION}.tar.gz" && \
	tar zxvf unbound*.tar.gz && \
	cd $(find . -type d -name 'unbound*') && \
	ln -s /usr/bin/python3 /usr/bin/python && \
	./configure \
    --with-deprecate-rsa-1024 \
    --with-libevent \
    --with-libnghttp2 \
    --enable-tfo-server \
    --enable-tfo-client \
    --with-ssl=/opt/openssl \
    --with-pythonmodule \
    --with-pyunbound  && \
	make && \
	make install && \
	useradd unbound && \
	chown -R unbound: /usr/local/etc/unbound/ && \
	cd /opt && \
	rm -Rf /opt/unbound*

# RUN git clone https://github.com/facebookresearch/fastText.git && \
    # cd fastText && \
    # pip3 install . -t /usr/lib/python3/dist-packages && \
RUN pip3 install sklearn requests grpcio protobuf websocket-client asyncio fasttext-wheel -t  /usr/lib/python3/dist-packages


RUN apt-get purge -y build-essential \
	libevent-dev \
	# libpython3.6-dev \
	libssl-dev \
	swig \
	wget

WORKDIR /usr/local/etc/unbound
RUN mv unbound.conf unbound.conf.org
COPY data/unbound.sh ./
RUN chmod +x ./unbound.sh

COPY data/opt/unbound/etc/unbound/python/dns_fasttext.py ./
COPY data/opt/unbound/etc/unbound/python/ai_util.py ./
COPY data/opt/unbound/etc/unbound/python/ai_dns.py ./
COPY data/opt/unbound/etc/unbound/python/ai_list.py ./
COPY data/opt/unbound/etc/unbound/python/list_pb2_grpc.py ./
COPY data/opt/unbound/etc/unbound/python/list_pb2.py ./

COPY data/opt/unbound/etc/unbound/forward-records.conf ./

COPY data/opt/unbound/etc/unbound/mldns/model.bin ./
COPY data/opt/unbound/etc/unbound/mldns/pca.pkl ./
COPY data/opt/unbound/etc/unbound/mldns/config.json ./

COPY data/opt/unbound/etc/unbound/root.key ./var/

RUN chmod +x ./*.py


EXPOSE 53/tcp
EXPOSE 53/udp
EXPOSE 10953/tcp

ENTRYPOINT ["./unbound.sh"]