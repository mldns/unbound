# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: list.proto
"""Generated protocol buffer code."""
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='list.proto',
  package='listManagement',
  syntax='proto3',
  serialized_options=b'Z\021list-service/grpc',
  create_key=_descriptor._internal_create_key,
  serialized_pb=b'\n\nlist.proto\x12\x0elistManagement\">\n\rDomainRequest\x12\x0e\n\x06\x64omain\x18\x01 \x01(\t\x12\x0e\n\x06status\x18\x02 \x01(\x05\x12\r\n\x05level\x18\x03 \x01(\x05\"?\n\x0e\x44omainResponse\x12\x0e\n\x06\x64omain\x18\x01 \x01(\t\x12\x0e\n\x06status\x18\x02 \x01(\x05\x12\r\n\x05level\x18\x03 \x01(\x05\"!\n\x0e\x44\x65leteResponse\x12\x0f\n\x07success\x18\x01 \x01(\x08\x32\xf0\x01\n\x0eListManagement\x12H\n\x05\x43heck\x12\x1d.listManagement.DomainRequest\x1a\x1e.listManagement.DomainResponse\"\x00\x12I\n\x06Insert\x12\x1d.listManagement.DomainRequest\x1a\x1e.listManagement.DomainResponse\"\x00\x12I\n\x06\x44\x65lete\x12\x1d.listManagement.DomainRequest\x1a\x1e.listManagement.DeleteResponse\"\x00\x42\x13Z\x11list-service/grpcb\x06proto3'
)




_DOMAINREQUEST = _descriptor.Descriptor(
  name='DomainRequest',
  full_name='listManagement.DomainRequest',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='domain', full_name='listManagement.DomainRequest.domain', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='status', full_name='listManagement.DomainRequest.status', index=1,
      number=2, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='level', full_name='listManagement.DomainRequest.level', index=2,
      number=3, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=30,
  serialized_end=92,
)


_DOMAINRESPONSE = _descriptor.Descriptor(
  name='DomainResponse',
  full_name='listManagement.DomainResponse',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='domain', full_name='listManagement.DomainResponse.domain', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='status', full_name='listManagement.DomainResponse.status', index=1,
      number=2, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='level', full_name='listManagement.DomainResponse.level', index=2,
      number=3, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=94,
  serialized_end=157,
)


_DELETERESPONSE = _descriptor.Descriptor(
  name='DeleteResponse',
  full_name='listManagement.DeleteResponse',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='success', full_name='listManagement.DeleteResponse.success', index=0,
      number=1, type=8, cpp_type=7, label=1,
      has_default_value=False, default_value=False,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=159,
  serialized_end=192,
)

DESCRIPTOR.message_types_by_name['DomainRequest'] = _DOMAINREQUEST
DESCRIPTOR.message_types_by_name['DomainResponse'] = _DOMAINRESPONSE
DESCRIPTOR.message_types_by_name['DeleteResponse'] = _DELETERESPONSE
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

DomainRequest = _reflection.GeneratedProtocolMessageType('DomainRequest', (_message.Message,), {
  'DESCRIPTOR' : _DOMAINREQUEST,
  '__module__' : 'list_pb2'
  # @@protoc_insertion_point(class_scope:listManagement.DomainRequest)
  })
_sym_db.RegisterMessage(DomainRequest)

DomainResponse = _reflection.GeneratedProtocolMessageType('DomainResponse', (_message.Message,), {
  'DESCRIPTOR' : _DOMAINRESPONSE,
  '__module__' : 'list_pb2'
  # @@protoc_insertion_point(class_scope:listManagement.DomainResponse)
  })
_sym_db.RegisterMessage(DomainResponse)

DeleteResponse = _reflection.GeneratedProtocolMessageType('DeleteResponse', (_message.Message,), {
  'DESCRIPTOR' : _DELETERESPONSE,
  '__module__' : 'list_pb2'
  # @@protoc_insertion_point(class_scope:listManagement.DeleteResponse)
  })
_sym_db.RegisterMessage(DeleteResponse)


DESCRIPTOR._options = None

_LISTMANAGEMENT = _descriptor.ServiceDescriptor(
  name='ListManagement',
  full_name='listManagement.ListManagement',
  file=DESCRIPTOR,
  index=0,
  serialized_options=None,
  create_key=_descriptor._internal_create_key,
  serialized_start=195,
  serialized_end=435,
  methods=[
  _descriptor.MethodDescriptor(
    name='Check',
    full_name='listManagement.ListManagement.Check',
    index=0,
    containing_service=None,
    input_type=_DOMAINREQUEST,
    output_type=_DOMAINRESPONSE,
    serialized_options=None,
    create_key=_descriptor._internal_create_key,
  ),
  _descriptor.MethodDescriptor(
    name='Insert',
    full_name='listManagement.ListManagement.Insert',
    index=1,
    containing_service=None,
    input_type=_DOMAINREQUEST,
    output_type=_DOMAINRESPONSE,
    serialized_options=None,
    create_key=_descriptor._internal_create_key,
  ),
  _descriptor.MethodDescriptor(
    name='Delete',
    full_name='listManagement.ListManagement.Delete',
    index=2,
    containing_service=None,
    input_type=_DOMAINREQUEST,
    output_type=_DELETERESPONSE,
    serialized_options=None,
    create_key=_descriptor._internal_create_key,
  ),
])
_sym_db.RegisterServiceDescriptor(_LISTMANAGEMENT)

DESCRIPTOR.services_by_name['ListManagement'] = _LISTMANAGEMENT

# @@protoc_insertion_point(module_scope)
