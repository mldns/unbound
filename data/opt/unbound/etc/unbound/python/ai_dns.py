from pickle import load
import time

import ai_util
import fasttext


class AiDns:
    """Artificial Intelligence (AI) Domain Name System (DNS)"""

    def __init__(self, dir, threshold):
        # Load model
        self.threshold = threshold

        self.model = fasttext.load_model(f'{dir}/model.bin')

        self.pca = load(open(f'{dir}/pca.pkl', 'rb'))

        self.goodLogger = ai_util.setup_logger("good", f'{dir}/logs/good.log')
        self.badLogger = ai_util.setup_logger("bad", f'{dir}/logs/bad.log')

    def prepare_domain(self, name):
        # strip domain
        return name \
            .replace('.', ' ') \
            .replace('-', ' ') \
            .replace('_', ' ')

    def check_domain(self, qinfo):
        # ask fasttext for prediction and apply pca
        start_time = time.time()

        sentence_vector = [self.model.get_sentence_vector(
            self.prepare_domain(qinfo.qname_str)
        )]

        pca_data = self.pca.transform(sentence_vector)

        result = pca_data[0][0] >= self.threshold

        if not result:
            self.badLogger.info("Predicted {} for {} in {} with certainty of {}".format(
                qinfo.qname_str, qinfo.qtype_str, (time.time() - start_time), pca_data[0][0]))

        else:
            self.goodLogger.info("Predicted {} for {} in {} with certainty of {}".format(
                qinfo.qname_str, qinfo.qtype_str, (time.time() - start_time), pca_data[0][0]))

        return result

        # return true
