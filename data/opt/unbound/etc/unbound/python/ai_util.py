import logging
import socket
import os


def setup_logger(name, log_file, level=logging.INFO):
    # create the custom logger
    handler = logging.FileHandler(log_file)
    handler.setFormatter(logging.Formatter(
        fmt="%(asctime)s: (%(filename)s): %(levelname)s: %(message)s"
    ))

    logger = logging.getLogger(name)
    logger.setLevel(level)
    logger.addHandler(handler)

    return logger


def get_ip_v4():
    # get the local ipv4 address (for response)
    try:
        try:
            socket.inet_aton(os.environ['HOST_IPV4'])
            return os.environ['HOST_IPV4']
        except OSError:
            return socket.gethostbyname(os.environ['HOST_IPV4'])
    except KeyError:
        st = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        try:
            st.connect(('10.255.255.255', 1))
            return st.getsockname()[0]
        except Exception:
            return '127.0.0.1'
        finally:
            st.close()


def get_ip_v6():

    # try:
    #   return os.environ['HOST_IPV6']
    # except KeyError:
    # get ipv6 address (for response)
    st = socket.socket(socket.AF_INET6, socket.SOCK_DGRAM)
    try:
        st.connect(('2001:0db8:85a3:0000:0000:8a2e:0370:7334', 1))
        return st.getsockname()[0]
    except Exception:
        return 'fe80::1'
    finally:
        st.close()
