from enum import Enum
import requests
import grpc

import list_pb2
import list_pb2_grpc

from websocket import create_connection

class Status(Enum):
   # Enum for the status, mapped in backend
    Blocked = 1
    Allowed = 2
    Unknown = 3


class Level(Enum):
    # Enum for the level, mapped in backend
    Personal = 1
    Provider = 2
    MLDNS = 3


def ask_server(domain) -> Status:
    # make a api call to backend and determin by status code if domain is blocked
    code = requests.get(
        "http://list:80/api/lists/"+domain).status_code
    if code == 200:
        return Status.Allowed
    elif code == 400:
        return Status.Blocked
    else:
        return Status.Unknown


def get_config():
    r = requests.get("http://list:80/api/config")
    return r.json()

def add_prediction_to_mldns_list(domain, status):
    # make api call to backend and set domain with status on mldns list
    requests.post("http://list:80/api/lists",
                  json={"domain": domain, "status": status.value, "level": Level.MLDNS.value})


def prepare_grpc():
    channel = grpc.insecure_channel('list:8080')
    global stub
    stub = list_pb2_grpc.ListManagementStub(channel)

def ask_grpc(domain) -> Status:
    request = list_pb2.DomainRequest(domain=domain)
    domainResponse = stub.Check(request)
    if domainResponse.status == 1:
        return Status.Blocked
    elif domainResponse.status == 2:
        return Status.Allowed
    else:
        return Status.Unknown


def prepare_ws():
    global ws
    ws = create_connection("ws://list:8081/api/lists/ws")

def teardown_ws():
    ws.close()

def ask_ws(domain) -> Status:
    ws.send(domain)
    response =  ws.recv()
    if response == "1":
        return Status.Blocked
    elif response == "2":
        return Status.Allowed
    else:
        return Status.Unknown

