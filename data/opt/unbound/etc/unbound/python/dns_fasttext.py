import json
import os
from ai_dns import AiDns
import ai_util
import ai_list
import time


# Settings
THRESHOLD = 0.02
ANONYMOUS_DATA = False
ACTIVE = True
DIR = os.path.dirname(os.path.realpath(__file__))


def read_config():
    obj = ai_list.get_config()

    global THRESHOLD
    THRESHOLD = float(obj.get('threshold', THRESHOLD))
    global ACTIVE
    ACTIVE = bool(obj.get('active', ACTIVE))
    global ANONYMOUS_DATA
    ANONYMOUS_DATA = bool(obj.get('anonymous_data', ANONYMOUS_DATA))


def prepare_blocked_repsonse(qstate, id):
    # function to create the response for a given type. Currently many code duplicates to be populated in the future with different behavior if needed
    msg = DNSMessage(qstate.qinfo.qname_str, qstate.qinfo.qtype,
                     RR_CLASS_IN, PKT_QR | PKT_RA | PKT_AA)

    # append RR
    if (qstate.qinfo.qtype == RR_TYPE_A):
        msg.answer.append("%s 1 IN A %s" %
                          (qstate.qinfo.qname_str, ip_addr_v4))
        qstate.return_rcode = RCODE_NOERROR

    elif (qstate.qinfo.qtype == RR_TYPE_AAAA):
        msg.answer.append("%s 1 IN AAAA %s" %
                          (qstate.qinfo.qname_str, ip_addr_v6))
        qstate.return_rcode = RCODE_NOERROR

    elif (qstate.qinfo.qtype == RR_TYPE_MX):
        qstate.return_rcode = RCODE_NXDOMAIN

    elif (qstate.qinfo.qtype == RR_TYPE_TXT):
        qstate.return_rcode = RCODE_NXDOMAIN

    elif (qstate.qinfo.qtype == RR_TYPE_CNAME):
        qstate.return_rcode = RCODE_NXDOMAIN

    elif (qstate.qinfo.qtype == RR_TYPE_ANY):
        qstate.return_rcode = RCODE_NXDOMAIN

    elif (qstate.qinfo.qtype == RR_TYPE_PTR):
        qstate.return_rcode = RCODE_NXDOMAIN

    elif (qstate.qinfo.qtype == RR_TYPE_SRV):
        qstate.return_rcode = RCODE_NXDOMAIN

    else:
        qstate.return_rcode = RCODE_NXDOMAIN

    # set qstate.return_msg
    if not msg.set_return_msg(qstate):
        qstate.ext_state[id] = MODULE_ERROR
        return True

    # we don't need validation, result is valid
    qstate.return_msg.rep.security = 2

    qstate.ext_state[id] = MODULE_FINISHED


def check_correct_type(qtype):
    # check if the current query type should be predicted (checked with the backend)
    return (
        qtype == RR_TYPE_A or
        qtype == RR_TYPE_AAAA or
        qtype == RR_TYPE_MX or
        qtype == RR_TYPE_TXT or
        qtype == RR_TYPE_CNAME or
        qtype == RR_TYPE_ANY or
        qtype == RR_TYPE_PTR or
        qtype == RR_TYPE_SRV
    )


def init_standard(id, env):
    # interface init method from unbound
    log_info("pythonmod: fasttext init")

    read_config()

    if not ACTIVE:
        log_info("pythonmod: fasttext not active")
        return True

    global ai_DNS
    ai_DNS = AiDns(DIR, THRESHOLD)

    ai_list.prepare_grpc()
    ai_list.prepare_ws()

    # Get local IP address
    global ip_addr_v4
    ip_addr_v4 = ai_util.get_ip_v4()
    log_info("pythonmod: ipv4: "+ ip_addr_v4)

    global ip_addr_v6
    ip_addr_v6 = ai_util.get_ip_v6()
    log_info("pythonmod: ipv6: "+ ip_addr_v6)

    return True


def deinit(id):
    # interface deinit method from unbound
    ai_list.teardown_ws()
    log_info("pythonmod: fasttext deinit")
    return True


def operate(id, event, qstate, qdata):
    log_info("pythonmod: operate " +qstate.qinfo.qname_str + " for " +str(event))

    # interface operate method from unbound. Gets called by every query, even recursively for some types
    if (event == MODULE_EVENT_NEW) or (event == MODULE_EVENT_PASS):

        # only go on if the feature is turned on and correct query type
        if ACTIVE and check_correct_type(qstate.qinfo.qtype):
            start_time = time.time()
            status = ai_list.ask_server(qstate.qinfo.qname_str)
            end_time = time.time()
            log_info("pythonmod: ask_server took " +str((end_time - start_time)*1000)+ "ms with result of "+ str(status))

            start_time = time.time()
            status = ai_list.ask_grpc(qstate.qinfo.qname_str)
            end_time = time.time()
            log_info("pythonmod: ask_grcp took " +str((end_time - start_time)*1000)+ "ms with result of "+ str(status))

            start_time = time.time()
            # status = asyncio.run(ai_list.ask_ws(qstate.qinfo.qname_str))
            ai_list.ask_ws(qstate.qinfo.qname_str)
            end_time = time.time()
            log_info("pythonmod: ask_ws took " +str((end_time - start_time)*1000)+ "ms with result of "+ str(status))


            if status == ai_list.Status.Blocked:
                # perpare a special response because domain is on blocklist
                prepare_blocked_repsonse(qstate, id)
                return True

            elif status == ai_list.Status.Allowed:
                # pass the query to validator because is on allowlist
                qstate.ext_state[id] = MODULE_WAIT_MODULE
                return True

            elif status == ai_list.Status.Unknown:
                # if backend does not have this domain in any list
                if not ai_DNS.check_domain(qstate.qinfo):
                    # if ai predicted malicious domain
                    prepare_blocked_repsonse(qstate, id)
                    # add domain with prediction to backend
                    ai_list.add_prediction_to_mldns_list(
                        qstate.qinfo.qname_str, ai_list.Status.Blocked)
                    return True
                else:
                    ai_list.add_prediction_to_mldns_list(
                        qstate.qinfo.qname_str, ai_list.Status.Allowed)
                    # pass the query to validator because ai predicted good domain
                    qstate.ext_state[id] = MODULE_WAIT_MODULE
                    return True
            else:
                # default case if mldns-service delivers wrong/not known status (sign for different unbound-plugin and mldns-service versions)
                log_err("pythonmod: wrong status from mldns-service")

        # pass the query to validator
        qstate.ext_state[id] = MODULE_WAIT_MODULE
        return True

    elif event == MODULE_EVENT_MODDONE:  # the iterator has finished
        # we don't need modify result
        qstate.ext_state[id] = MODULE_FINISHED
        return True

    log_err("pythonmod: Unknown event")
    qstate.ext_state[id] = MODULE_ERROR
    return True


def inform_super(id, qstate, superqstate, qdata):
    # interface method from unbound
    return True
